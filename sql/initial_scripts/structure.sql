create database todo_db;

create user 'adminimdrUBX'@'localhost' identified by 'qd9IWtuVxxaL';
grant all on todo_db.* to 'adminimdrUBX'@'localhost' identified by 'qd9IWtuVxxaL';

use todo_db;

CREATE TABLE test_table (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;