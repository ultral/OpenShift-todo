package pl.blachnio.todo.servlets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.blachnio.todo.Logging;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = RandomServlet.SERVLET_NAME,
        urlPatterns = {"/" + RandomServlet.SERVLET_NAME})
public class RandomServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(RandomServlet.class);
    public static final String SERVLET_NAME = "random";

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        process(response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        process(response);
    }

    private static final List<String> data = new ArrayList<String>(Arrays.asList(
            "czuk", "bydlak", "ultral"));

    private void process(HttpServletResponse response) {
        Logging.info(logger, "Called servlet: " + SERVLET_NAME);
        String redirectUrl = "https://www.google.pl/#q="+data.get(((Long)(System.currentTimeMillis()%data.size())).intValue());

        try {
            response.sendRedirect(redirectUrl);
        } catch (IOException e) {
            Logging.error(logger, "Error during redirecting: " + redirectUrl, e);
        }
    }

}




