package pl.blachnio.todo.servlets;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.blachnio.todo.Logging;
import pl.blachnio.todo.model.TestTableModel;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = DbServlet.SERVLET_NAME,
        urlPatterns = {"/" + DbServlet.SERVLET_NAME, "/" + DbServlet.SERVLET_NUM})
public class DbServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(DbServlet.class);
    public static final String SERVLET_NAME = "db";
    public static final String SERVLET_NUM = "dbnum";
    private static final String ID = "id";

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        process(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        process(request, response);
    }

    private void processDb(HttpServletRequest request, HttpServletResponse response) {
        SessionFactory sessionFactory = (SessionFactory) request.getServletContext().getAttribute("SessionFactory");

        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        String rowsNum = session.createSQLQuery("SELECT count(*) FROM test_table").uniqueResult().toString();
        //the same as above: session.createCriteria(TestTableModel.class).setProjection(Projections.rowCount()).uniqueResult().toString();

        TestTableModel ttm = new TestTableModel("entry number: "+rowsNum);
        session.saveOrUpdate(ttm);
        tx.commit();

        try {
            Writer writer = response.getWriter();
            writer.write("Inserted: "+ (Integer.parseInt(rowsNum) + 1) +" record into database.");
        } catch (IOException e) {
            Logging.error(logger, "Error during inserting into database: " + getClass().getPackage().getImplementationVersion(), e);
        }
    }

    private void processNum(HttpServletRequest request, HttpServletResponse response) {
        Map<String,String[]> parameterMap = request.getParameterMap();

        String message;
        Gson gson = new Gson();
        SessionFactory sessionFactory = (SessionFactory) request.getServletContext().getAttribute("SessionFactory");
        Session session = sessionFactory.getCurrentSession();

        if (parameterMap.containsKey(ID) && parameterMap.get(ID).length > 0 && parameterMap.get(ID)[0] != null &&
            !parameterMap.get(ID)[0].equals("")) {

            int idNum;
            try {
                idNum = Integer.parseInt(parameterMap.get(ID)[0]);
                Transaction tx = session.beginTransaction();
                TestTableModel ttm = (TestTableModel) session.get(TestTableModel.class, idNum);
                tx.commit();
                message = gson.toJson(ttm);
            } catch (NumberFormatException e) {
                message = "Wrong number format";
                Logging.info(logger, "Wrong number format during entry retrieval");
            }

        } else {
            Transaction tx = session.beginTransaction();
            final Integer rowsNum = Integer.valueOf(session.createSQLQuery("SELECT count(*) FROM test_table").uniqueResult().toString());
            tx.commit();
            Map<String, Integer> map = new HashMap<String, Integer>(){{put("Entries number", rowsNum);}};
            message = gson.toJson(map, new TypeToken<Map<String, Integer>>(){}.getType());
        }

        try {
            Writer writer = response.getWriter();
            writer.write(message);
        } catch (IOException e) {
            Logging.error(logger, "Error during reading entries from database: ", e);
        }
    }


    private void process(HttpServletRequest request, HttpServletResponse response) {
        Logging.info(logger, "Called uri: " + request.getRequestURI());

        if (request.getRequestURI().endsWith(SERVLET_NAME)) {
            processDb(request, response);
        } else if (request.getRequestURI().endsWith(SERVLET_NUM)) {
            processNum(request, response);
        }
    }

}




