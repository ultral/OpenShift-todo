package pl.blachnio.todo.servlets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.blachnio.todo.Logging;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

@WebServlet(name = AppVersionServlet.SERVLET_NAME,
        urlPatterns = {"/" + AppVersionServlet.SERVLET_NAME})
public class AppVersionServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(AppVersionServlet.class);
    public static final String SERVLET_NAME = "appVersion";

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        process(response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        process(response);
    }

    private void process(HttpServletResponse response) {
        Logging.info(logger, "Called servlet: " + SERVLET_NAME);
        try {
            Writer writer = response.getWriter();
            writer.write("Application Version: " + getClass().getPackage().getImplementationVersion());
        } catch (IOException e) {
            Logging.error(logger, "Error during app version: " + getClass().getPackage().getImplementationVersion() + " writing.", e);
        }
    }

}




