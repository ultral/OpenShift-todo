package pl.blachnio.todo;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

public class Logging {

    public static void log (final Logger logger, final String message, final Exception e, final Level level){
        if (logger.isEnabled(level)) {
            logger.log(level, message, e);
        }
    }

    public static void log (final Logger logger, final String message, final Level level){
        if (logger.isEnabled(level)) {
            logger.log(level, message);
        }
    }

    public static void info (final Logger logger, final String message){
        log(logger, message, Level.INFO);
    }

    public static void info (final Logger logger, final String message, final Exception e){
        log(logger, message, e, Level.INFO);
    }

    public static void warn (final Logger logger, final String message){
        log(logger, message, Level.WARN);
    }

    public static void warn (final Logger logger, final String message, final Exception e){
        log(logger, message, e, Level.WARN);
    }

    public static void error (final Logger logger, final String message){
        log(logger, message, Level.ERROR);
    }

    public static void error (final Logger logger, final String message, final Exception e){
        log(logger, message, e, Level.ERROR);
    }
}
