package pl.blachnio.todo.model;

import javax.persistence.*;

@Entity
@Table(name="test_table",
        uniqueConstraints={@UniqueConstraint(columnNames={"ID"})})
public class TestTableModel {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ID", nullable=false, unique=true, length=11)
    private int id;

    @Column(name="NAME", length=20, nullable=true)
    private String name;

    public TestTableModel(){}
    public TestTableModel(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return "id : '" + id + "', name : '" + name + "'";
    }
}
